#!/bin/bash

while getopts r:u:m:p:n:d:k:c:v:b: option 
do 
case "${option}" 
	in 
	r) route=${OPTARG};;
	u) site_url=${OPTARG};;
	m) mysql_bin=${OPTARG};;
	p) prefix=${OPTARG};;
	n) project_name=${OPTARG};;
	d) upload_dir=${OPTARG};;
	k) key=${OPTARG};;
	c) odb=${OPTARG};;
	v) userdb=${OPTARG};;
	b) userpass=${OPTARG};;
esac 
done 


echo project_name: $project_name
echo userdb: $userdb
echo userpass: $userpass
echo route: $route
echo mysql_bin: $mysql_bin
echo site_url: $site_url
echo database: $odb
echo site_url: $site_url
echo prefix: $prefix
echo upload_dir: $upload_dir
echo key: $key
echo odb: $odb
echo userdb: $userdb
echo userpass: $userpass

cd $route
mkdir massive_file
cp -r $project_name massive_file/filem
rm -r massive_file/filem/wp-content/uploads/pete-converter/*

echo "domain: '$site_url'
platform: 'WordPress'
prefix: '$prefix'
" > $route/massive_file/config.txt

#echo "$mysql_bin --host=localhost -u$userdb -p$userpass $odb > $upload_dir/massive_file/query.sql"
$mysql_bin --host=localhost -u$userdb -p$userpass $odb > $route/massive_file/query.sql

#tar czf massive_file.tar.gz massive_file
zip -r massive_file.zip massive_file
mv massive_file.zip $upload_dir/$project_name$key.zip
cd $route && rm -rf massive_file

