<?php
/*
Plugin Name: WordPress Pete Converter
Plugin URI: https://wordpresspete.com
Description: Quickly and easily export your WordPress installation to WordPress Pete format. WordPressPete is a local and production server environment that can be installed in macOS, Windows, Linux and Docker with just a few clicks. Migrating, launching, integrating, or cloning a WordPress site has never been easier.
Version: 1.1
Author: Pedro Consuegra
Author URI: https://wordpresspete.com
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html
*/

require_once(ABSPATH . 'wp-admin/includes/file.php');
//require_once(ABSPATH. '/wp-config.php');
include_once(dirname(__FILE__) . '/bin/Mysqldump.php');

if ( ! function_exists('write_log')) {
   function write_log ( $log )  {
      if ( is_array( $log ) || is_object( $log ) ) {
         error_log( print_r( $log, true ) );
      } else {
         error_log( $log );
      }
   }
}


if ( ! function_exists('get_table_prefix')) {
function get_table_prefix() {
       global $wpdb;
       return $wpdb->prefix;
}
}

function theme_options_panel(){
  add_menu_page('Theme page title', 'Pete Converter', 'manage_options', 'pete-export-options', 'pete_export_view',plugins_url('pete-converter/petefaceicon.png', dirname(__FILE__) ),6 );
 
}
add_action('admin_menu', 'theme_options_panel');
function wps_theme_func(){ echo '<div class="wrap"><div id="icon-options-general" class="icon32"><br></div> <h2>Theme</h2></div>'; } 



function pete_export_view() { ?>

	<div class="wrap"><h1>Export site to WordPress Pete format</h1> <br />
		
		<p>Increase the PHP max_execution_time variable to avoid error response. You can increase PHP max_execution_time in three ways: </p>
		
		<ol>
			<li>Add/edit the following line in the wp-config.php of your WordPress Editing wp-config.php. set_time_limit(300); </li>
			<li>Edit the websites php.ini file to increase the Maximum Execution Times. max_execution_time = 300</li>
			<li>In the root of your WordPress folder add these vairables to your .htaccess file: php_value max_execution_time 300</li>
		</ol>
		
		<?php
		    echo '<form action="'.admin_url('/admin.php?page=pete-export-options').'" method="post">';
		?>
	
	<input class='button-primary' type='submit' name="pete-converter" id="pete-converter" value='Export site'>
	<?php wp_nonce_field('export_with_bash','pete_nonce_field'); ?>
	</form>
	
	</div>
	

<?php }

function recurse_copy($src,$dst) {
    $dir = opendir($src);
    @mkdir($dst);
    while(false !== ( $file = readdir($dir)) ) {
        if (( $file != '.' ) && ( $file != '..' )) {
			
			if((!strpos($src, "massive_file") !== false) && ($file != ".htaccess")){
				
	            if ( is_dir($src . '/' . $file)) {
	                recurse_copy($src . '/' . $file,$dst . '/' . $file);
	            }
	            else {
	                stream_copy($src . '/' . $file,$dst . '/' . $file);
	            }
			}
			
        }
    }
    closedir($dir);
}

function stream_copy($src, $dest){
	$fsrc = fopen($src,'r');
	$fdest = fopen($dest,'w+');
	$len = stream_copy_to_stream($fsrc,$fdest);
	fclose($fsrc);
	fclose($fdest);
	return $len;
}
	
	
function rrmdir($dir) {
   if (is_dir($dir)) {
     $objects = scandir($dir);
     foreach ($objects as $object) {
       if ($object != "." && $object != "..") {
         if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object);
       }
     }
     reset($objects);
     rmdir($dir);
   }
}

function oclean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

add_action('plugins_loaded','pete_converter_action');

function pete_converter_action(){
	
if(isset($_POST['pete-converter'])) { 
	
    if ( !empty($_POST['pete-converter']) && check_admin_referer('export_with_bash', 'pete_nonce_field' ) && ( current_user_can( 'manage_options' ))){
		
		
		//SETUP FILES VARIABLES
		$root_path = get_home_path();			
		$previous_file = $root_path."massive_file.zip";
		write_log("previous_file: $previous_file");
		if (file_exists($previous_file)) {
			unlink($previous_file);
		}
		
		//CREATING FILEM FOLDER	
		$pete_converter_folder=$root_path;
		if(!is_dir( $pete_converter_folder))
			mkdir($pete_converter_folder, 0700);
		$massive_folder = $root_path."massive_file";
		if(!is_dir( $massive_folder))
			mkdir($massive_folder, 0700);
		recurse_copy($root_path,"$massive_folder/filem");
		
		//CREATING SQL FILE
		$db_name = constant('DB_NAME');
		$db_user = constant('DB_USER');
		$db_password = constant('DB_PASSWORD');
		$db_host = constant('DB_HOST');
		$dump = new Ifsnop\Mysqldump\Mysqldump("mysql:host=$db_host;dbname=$db_name", $db_user, $db_password);
		$dump->start("$massive_folder/query.sql");
		
		//CREATING config.txt
		global $wpdb;
		$site_url = get_site_url();
		$site_url = str_replace('http://','', $site_url);
		$site_url = str_replace('https://','', $site_url);
		$site_url_content = "domain: '$site_url'".PHP_EOL;
		$platform_content = "platform: 'WordPress'".PHP_EOL;
		$prefix_content= "prefix: '$wpdb->prefix'".PHP_EOL;
		
		$fp = fopen($massive_folder . "/config.txt","wb");
		fwrite($fp,$site_url_content);
		fwrite($fp,$platform_content);
		fwrite($fp,$prefix_content);
		fclose($fp);		
		
		//CREATING ZIP FILE
		$massive_file_path = "$pete_converter_folder/massive_file.zip";
		if (file_exists($massive_file_path)) {
			unlink($massive_file_path);
		}
		$pd = new \PharData($massive_file_path);
		$pd->buildFromDirectory($massive_folder);
		rrmdir($massive_folder);
		
		//REDIRECT TO DOWNLOAD FILE
		wp_redirect("/massive_file.zip");
		
    }else{
        wp_die('Security check fail'); 
    }
}

}




?>
