=== Pete Converter ===
Contributors: peterconsuegra, ozone, laurabusche
Tags: akismet, clone WordPress, laravel, laravel Integration, laravel wordpress, export wordpress, install local
Requires at least: 4.0
Tested up to: 4.9.6
License: GPLv2 or later

== Description ==

Quickly and easily export your WordPress installation to WordPressPete format. Integrate Laravel, migrate, launch, or clone WordPress sites in minutes. WordPressPete is a local and production server environment that can be installed in macOS and Linux with just one command from the terminal.

Major features in Pete Converter:

* Convert your WordPress to WordpressPete format.
* Support for MAMP.

== Installation ==

Upload the WordPressPete Converter plugin to your WordPress, Activate it, then click in Pete Converter after that press the button Convert to WordPressPete.

1, 2, 3: You're done!

== Changelog ==

= 1.0 =
*Release Date - 2 October 2018*